from django.shortcuts import render, redirect
from .forms import Status
from .models import ModelStatus
from datetime import datetime, date

# Create your views here.

# def index(request):
#     return render(request, 'index.html')

def status(request):
    obj = ModelStatus.objects.all()
    data = Status()
    if request.method == "POST":
        data = Status(request.POST)
        if data.is_valid():
            data.save()
            return redirect('appstory6:status')
    else :
        data = Status()
    return render(request, 'index.html', {'data' : data, 'obj': obj})

    
