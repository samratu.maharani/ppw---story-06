from django.db import models
import datetime

# Create your models here.

class ModelStatus(models.Model):
    status = models.CharField(max_length=300)
    date = models.DateTimeField(auto_now_add=True)
