from django .db import models
from django import forms
from .models import ModelStatus
from django.forms import ModelForm

import datetime

class Status(forms.ModelForm):
    status = forms.CharField(widget=forms.TextInput(
        attrs = {
            'size' : 50,
            'placeholder' : 'Apa Kabar?'
        } 
    ))
    class Meta:
        model = ModelStatus
        fields = ['status']
        labels = {'status' : 'status'}