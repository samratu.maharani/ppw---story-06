from django.test import TestCase, Client, LiveServerTestCase
from django.urls import resolve
from django.utils import timezone
from django.http import HttpRequest
import unittest
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from .views import *
from .models import ModelStatus
from .forms import Status
from datetime import date
import time

# Create your tests here.
class UrlTest(TestCase):
    def test_url_valid(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_index(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')

    def test_url_invalid(self):
        response = Client().get('/notexist/')
        self.assertEqual(response.status_code, 404)

class LandingPageTest(TestCase):
    def test_index_using_func(self):
        found = resolve('/')
        self.assertEqual(found.func, status)

    def test_index_contains_greeting(self):
        found = resolve('/')
        response = Client().get('')
        response_content = response.content.decode('utf-8')
        self.assertIn("Halo, Bagaimana Statusmu Hari Ini?", response_content)

class FormTest(TestCase):
    def test_forms_valid(self):
        form_data = {'status' :  'testsatus'}
        form = Status(data=form_data)
        self.assertTrue(form.is_valid())

    def test_forms_invalid(self):
        form_data = {'status' : 'hahahahaaahahhahahahahahahahahahhahahahahahahahhaahhahahahahahahhahahahahahahahahahahahahahahahhahahahahhahahahahahahahahahahhahahahahahahahahahahahahahhahahahahhahahahhahahhahahahaaahahhahahahahahahahahahhahahahahahahahhaahhahahahahahahhahahahahahahahahahahahahahahahhahahahahhahahahahahahahahahahhahahahahahahahahahahahahahhahahahahhahahahhahah'}
        form = Status(data=form_data)
        self.assertFalse(form.is_valid())

class ModelTest(TestCase):
    def test_model_addstatus(self):
        new_status = ModelStatus.objects.create(date=timezone.now(), status = 'apakekk')
        count_status_obj = ModelStatus.objects.all().count()
        self.assertEqual(count_status_obj, 1)
    
class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('--disable-gpu')
        self.selenium  = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(FunctionalTest, self).tearDown()

    def test_post(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)
        new_status = selenium.find_element_by_name('status')
        new_status.send_keys("tes status")
        time.sleep(5)
        submit = selenium.find_element_by_class_name('submit')
        submit.send_keys(Keys.RETURN)
        time.sleep(10)
        selenium.get(self.live_server_url)
        time.sleep(5)
        self.assertIn('tes status', selenium.page_source)



